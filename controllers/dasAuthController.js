'use strict';

var trainerDeatilsModal = require('../models/dasModel').trainerDeatilsModal;
var studentAttendanceModal = require('../models/dasModel').studentAttendanceModal;
var studentRegistrationModal = require('../models/dasModel').studentRegistrationModal;


var dasAuthController = {

    getStudentFpScan: function(req, res) {
        let searchId = new RegExp(req.id, 'i')
        studentRegistrationModal.find({ studentId: searchId }, function(err, studentData) {
            if (err) {
                return res.status(500).send({ message: 3001 });
            } else {
                if (studentData.length > 0) {
                    return res.status(200).json(studentData);
                } else {
                    return res.status(201).send({ message: "No user found" });
                }
            }
        })
    },

    registerStudent: function(req, res) {
        studentRegistrationModal.insertMany(req.registrationData, function(err, registrationData) {
            if (err) {
                return res.status(500).send({ message: 3002 });
            } else {
                return res.status(200).send({ message: registrationData.length + "records inserted" });
            }
        })
    },

    studentAttendance: function(req, res) {
        studentAttendanceModal.insertMany(req.attendanceData, function(err, attendanceData) {
            if (err) {
                return res.status(500).send({ message: 3003 });
            } else {
                return res.status(200).send({ message: attendanceData.length + "records inserted" });
            }
        })
    },

    sync: function(req, res) {
        trainerDeatilsModal.find({}, function(err, trainerData) {
            if (err) {
                return res.status(500).send({ message: 3004 });;
            } else {
                if (trainerData.length > 0) {
                    return res.status(200).json(trainerData);
                } else {
                    return res.status(201).send({ message: "No user found" });
                }
            }
        })
    }
}

module.exports = dasAuthController;