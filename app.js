var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var logger = require('morgan');
var dbUrl = 'mongodb+srv://dasSarthak:dasSarthak110597@cluster0-c3j4a.mongodb.net/test?retryWrites=true';

var app = express();

var mongoOptions = {
    db: { safe: true },
    server: {
        reconnectInterval: 2000,
        auto_reconnect: true,
        reconnectTries: 60,
    }
};

app.use(express.static(__dirname + '/public'));

app.use(bodyParser.urlencoded({
   extended: true;
}));

app.use(bodyParser.json());

var auth = require('./routes/dasAuth');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/das', auth);
app.all('/*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Request-Method', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
    if (req.method == 'OPTIONS') {
        res.status(200).end();
    } else {
        next();
    }
});

mongoose.connect(dbUrl, mongoOptions, function(err, res) {
    if (err) {
        console.log('ERROR connecting to: ' + dbUrl + '. ' + err);
    } else {
        console.log('Successfully connected to: ' + dbUrl);
    }
});

mongoose.connection.on('open', function(ref) {
    console.log('open connection to mongo server.');
});

mongoose.connection.on('connected', function(ref) {
    console.log('connected to mongo server.');
});

mongoose.connection.on('disconnected', function(ref) {
    console.log('disconnected from mongo server.');
});

mongoose.connection.on('close', function(ref) {
    console.log('close connection to mongo server');
});

mongoose.connection.on('error', function(err) {
    console.log('error connection to mongo server!');
});

mongoose.connection.on('reconnect', function(ref) {
    console.log('reconnect to mongo server.');
});


module.exports = app;

app.set('port', process.env.PORT || 8877);

var server = app.listen(app.get('port'), function() {
    console.log('Express server listening on port ' + server.address().port);
});